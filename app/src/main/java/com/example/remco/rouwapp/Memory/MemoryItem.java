package com.example.remco.rouwapp.Memory;

/**
 * Created by Remco on 17-10-2016.
 */
public class MemoryItem {

    private String imageview;
private boolean visible;

    public MemoryItem(String image, boolean visible){
        this.imageview = image;
        this.visible = visible;
    }

    public String getImageview(){
        return imageview;
    }

    public void setVisible(boolean visible){
        this.visible = visible;
    }

    public boolean getVisible(){
        return visible;
    }

}
