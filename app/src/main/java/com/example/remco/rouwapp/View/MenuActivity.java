package com.example.remco.rouwapp.View;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.remco.rouwapp.CustomMenuItem;
import com.example.remco.rouwapp.Feeling;
import com.example.remco.rouwapp.ImagesDBHandler;
import com.example.remco.rouwapp.Memory.MainActivity;
import com.example.remco.rouwapp.MenuAdapter;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.R;
import com.example.remco.rouwapp.gridViewImageAdapter;
import com.example.remco.rouwapp.imageAdapter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Remco on 10-10-2016.
 */
public class MenuActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private Person p;
    private Context context;
    private GridView gridView;
    private ArrayList<CustomMenuItem> menuItems;
    private ArrayList<String> customImages;
    private Intent intent;
    private ImagesDBHandler idbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_menu);

        Bundle bundle = getIntent().getExtras();
        p = (Person) getIntent().getExtras().getSerializable("Person");


        idbh = new ImagesDBHandler(this);
        customImages = new ArrayList<>();
        menuItems = new ArrayList<>();

            genarateMenu();
            gridView = (GridView) findViewById(R.id.gridView);
            gridView.setAdapter(new MenuAdapter(this, menuItems));
            gridView.setOnItemClickListener(this);

        context = this;
    }

    @Override
    public void onBackPressed() {
            startActivity(new Intent(context, ListviewActivity.class));
    }

    private void getImages(){
        Cursor cursor = idbh.getImages(p.getUserID());
        for(int i = 0;i<cursor.getCount();i++){
            cursor.moveToPosition(i);
            String image = cursor.getString(cursor.getColumnIndex("image"));
            customImages.add(image);
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (menuItems.get(position).getName()) {
                case "Mijn gevoel":
                    intent = new Intent(context, ListviewActivity.class);
                    intent.putExtra("Person", p);
                    intent.putExtra("From", getResources().getString(R.string.Feeling));
                    startActivity(intent);
                    break;
                case "Ouders":
                    startActivity(new Intent(context, ParentActivity.class));
                    break;
                case "Emoties":
                    startActivity(new Intent(context, MainActivity.class));
                    break;
                case "Herinneringen":
                    intent = new Intent(context, ListviewActivity.class);
                    intent.putExtra("Person", p);
                    intent.putExtra("From", "herinneringen");
                    startActivity(intent);
                    break;
                case "Kleurplaten":
                    intent = new Intent(context, ListviewActivity.class);
                    intent.putExtra("Person", p);
                    intent.putExtra("From", "kleurplaat");
                    startActivity(intent);
                    break;
                case "Afbeeldingen":
                    intent = new Intent(context, PictureActivity.class);
                    intent.putExtra("Person", p);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }


    private void addPicture(){
        customImages.add("");
    }
    private void genarateMenu() {
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.My) + " " + getResources().getString(R.string.Feeling), 0, false));
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.colouring), 1, false));
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.remindings), 2, false));
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.emotions), 3, false));
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.parents), 4, false));
        menuItems.add(new CustomMenuItem(getResources().getString(R.string.pictures), 5, false));
    }
}
