package com.example.remco.rouwapp.View;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.remco.rouwapp.Kleurplaat;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Remco on 11-10-2016.
 */
public class DrawingActivity extends Activity implements OnClickListener {

    private DrawingView dv;
    private Paint mPaint;
    private Button yellowButton;
    private Button greenButton;
    private Button redButton;
    private Button blueButton;
    private Button lightBlueButton;
    private Button purpleButton;
    private Button thinButton;
    private Button thickButton;
    private Button saveButton;
    private Button brownButton;
    private Button pinkButton;
    private Button blackButton;
    private Button whiteButton;
    private Button normalButton;
    private Button undoButton;
    private Button redoButton;
    private ColorDrawable buttonColor;
    private int strokeWidth = 50;
    private Kleurplaat kleurplaat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        kleurplaat = (Kleurplaat) getIntent().getExtras().getSerializable("KleurplaatID");
        int animationID = this.getResources().getIdentifier(kleurplaat.getKleurplaatID(), "drawable", this.getPackageName());

        setContentView(R.layout.activity_drawing);
        undoButton = (Button) findViewById(R.id.UndoButton);
        redoButton = (Button) findViewById(R.id.RedoButton);

        dv = new DrawingView(this, animationID);

        LinearLayout canvas = (LinearLayout) findViewById(R.id.canvas);
        canvas.addView(dv);

        purpleButton = (Button) findViewById(R.id.PurpleButton);
        purpleButton.setOnClickListener(this);

        lightBlueButton = (Button) findViewById(R.id.LightBlueButton);
        lightBlueButton.setOnClickListener(this);

        blueButton = (Button) findViewById(R.id.BlueButton);
        blueButton.setOnClickListener(this);

        redButton = (Button) findViewById(R.id.RedButton);
        redButton.setOnClickListener(this);

        greenButton = (Button) findViewById(R.id.GreenButton);
        greenButton.setOnClickListener(this);

        yellowButton = (Button) findViewById(R.id.YellowButton);
        yellowButton.setOnClickListener(this);

        brownButton = (Button) findViewById(R.id.BrownButton);
        brownButton.setOnClickListener(this);

        pinkButton = (Button) findViewById(R.id.PinkButton);
        pinkButton.setOnClickListener(this);

        blackButton = (Button) findViewById(R.id.BlackButton);
        blackButton.setOnClickListener(this);

        whiteButton = (Button) findViewById(R.id.WhiteButton);
        whiteButton.setOnClickListener(this);

        thickButton = (Button) findViewById(R.id.ThickButton);
        thickButton.setOnClickListener(this);

        thinButton = (Button) findViewById(R.id.ThinButton);
        thinButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.SaveButton);
        saveButton.setOnClickListener(this);

        normalButton = (Button) findViewById(R.id.NormalButton);
        normalButton.setOnClickListener(this);

        buttonColor = (ColorDrawable) purpleButton.getBackground();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(50);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BlueButton:
                buttonColor = (ColorDrawable) blueButton.getBackground();
                break;
            case R.id.GreenButton:
                buttonColor = (ColorDrawable) greenButton.getBackground();
                break;
            case R.id.YellowButton:
                buttonColor = (ColorDrawable) yellowButton.getBackground();
                break;
            case R.id.PurpleButton:
                buttonColor = (ColorDrawable) purpleButton.getBackground();
                break;
            case R.id.LightBlueButton:
                buttonColor = (ColorDrawable) lightBlueButton.getBackground();
                break;
            case R.id.RedButton:
                buttonColor = (ColorDrawable) redButton.getBackground();
                break;
            case R.id.BrownButton:
                buttonColor = (ColorDrawable) brownButton.getBackground();
                break;
            case R.id.PinkButton:
                buttonColor = (ColorDrawable) pinkButton.getBackground();
                break;
            case R.id.BlackButton:
                buttonColor = (ColorDrawable) blackButton.getBackground();
                break;
            case R.id.WhiteButton:
                buttonColor = (ColorDrawable) whiteButton.getBackground();
                break;
            case R.id.ThickButton:
                strokeWidth = 50;
                break;
            case R.id.NormalButton:
                strokeWidth = 30;
                break;
            case R.id.ThinButton:
                strokeWidth = 10;
                break;
            case R.id.SaveButton:
                storeImage(getBitmapFromView(dv));
                break;
        }
        mPaint.setColor(buttonColor.getColor());
        mPaint.setStyle(Paint.Style.FILL);
    }

    private void storeImage(Bitmap image) {
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        MediaStore.Images.Media.insertImage(getContentResolver(), image, "Colouring_" + timeStamp, timeStamp);
        Toast.makeText(this, "Succesvol opgeslagen", Toast.LENGTH_LONG).show();
    }


    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public class DrawingView extends View {

        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;
        Context context;
        private Paint circlePaint;
        private Path circlePath;
        private Bitmap icon;
        private ArrayList<Path> paths = new ArrayList<>();
        private ArrayList<Path> notRedrawPaths = new ArrayList<>();
        private ArrayList<Path> pathsUndo = new ArrayList<>();
        private HashMap<Path, Paint> pathsPaint = new HashMap<>();
        private HashMap<Path, Paint> pathsPaintUndo = new HashMap<>();
        private int width;
        private int height;
        private Bitmap image;

        public DrawingView(Context c, int imageID) {
            super(c);
            context = c;


            undoButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (paths.size() > 0) {
                        pathsUndo.add(paths.remove(paths.size() - 1));
                        invalidate();
                    }
                    else{
                        Toast.makeText(context, "Kan niet meer terug", Toast.LENGTH_LONG).show();
                    }
                }
            });
            redoButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pathsUndo.size() > 0) {
                        paths.add(pathsUndo.remove(pathsUndo.size() - 1));
                        invalidate();
                    }
                    else{
                        Toast.makeText(context, "Kan niet meer verder", Toast.LENGTH_LONG).show();
                    }
                }
            });
            icon = BitmapFactory.decodeResource(context.getResources(),
                    imageID);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            circlePaint = new Paint();
            circlePath = new Path();
            circlePaint.setAntiAlias(true);
            circlePaint.setColor(Color.BLUE);
            circlePaint.setStyle(Paint.Style.FILL);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            this.width = w;
            this.height = h;
            RectF targetRect = new RectF(0, 0, w, h);
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            mCanvas.drawColor(Color.WHITE);
            mCanvas.drawBitmap(icon, null, targetRect, null);
            icon = BitmapFactory.decodeResource(context.getResources(),
                    context.getResources().getIdentifier(kleurplaat.getKleurplaatID().toLowerCase() + "_overlay", "drawable", context.getPackageName()));

            if(icon != null)
                image = Bitmap.createScaledBitmap(icon, width, height, true);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            for (Path p : paths) {
                canvas.drawPath(p, pathsPaint.get(p));
            }
            for (Path p : notRedrawPaths) {
                canvas.drawPath(p, pathsPaint.get(p));
            }
            if(image != null)
            canvas.drawBitmap(image, 0, 0, null);

            Iterator itr = paths.iterator();
            while (itr.hasNext()) {
                Path p = (Path) itr.next();
                if (paths.size() > 100) {
                    notRedrawPaths.add(p);
                    itr.remove();
                }
            }
        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;

                circlePath.reset();
                mPaint.reset();
                mPaint.setColor(buttonColor.getColor());
                mPaint.setStyle(Paint.Style.FILL);
                mPaint.setStrokeWidth(strokeWidth);
                mPaint.setAntiAlias(true);
                mPaint.setDither(true);
                mPaint.setStrokeJoin(Paint.Join.ROUND);
                mPaint.setStrokeCap(Paint.Cap.ROUND);
                circlePath.addCircle(mX, mY, mPaint.getStrokeWidth(), Path.Direction.CW);
                paths.add(circlePath);
                pathsPaint.put(circlePath, mPaint);
                mPaint = new Paint();
                circlePath = new Path();
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            circlePath.reset();
            mPaint.reset();
            // commit the path to our offscreen
            // mCanvas.drawPath(mPath,  mPaint);
            //paths.add(circlePath);
            // kill this so we don't double draw
            mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }


    }
}