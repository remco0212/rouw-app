package com.example.remco.rouwapp;

import java.util.Comparator;

public class PersonCompare implements Comparator<Person> {
    public int compare(Person left, Person right) {
        return left.getFirstName().compareTo(right.getFirstName());
    }
}
