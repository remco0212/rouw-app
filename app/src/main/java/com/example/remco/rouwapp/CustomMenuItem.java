package com.example.remco.rouwapp;

/**
 * Created by Remco on 10-10-2016.
 */
public class CustomMenuItem {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public boolean isFeeling() {
        return feeling;
    }

    public void setFeeling(boolean feeling) {
        this.feeling = feeling;
    }

    private boolean feeling;



    public CustomMenuItem(String name, int id, boolean feeling){
        this.name = name;
        this.id = id;
        this.feeling = feeling;
    }
}
