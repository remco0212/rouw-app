package com.example.remco.rouwapp.Memory;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.remco.rouwapp.R;
import com.example.remco.rouwapp.View.MenuActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private GridView gridView;
    private ArrayList<MemoryItem> memoryItems;
    private HashMap<Integer, Integer> memory;
    private HashMap<Integer, Integer> memoryRemove;
    private int match = -1;
    private gridviewAdapter Adapter;
    private int clickCounter = 0;
    private Handler customHandler = new Handler();
    private TextView scoreboard;
    private int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scoreboard = (TextView) findViewById(R.id.scoreboard);
        memory = new HashMap<>();
        memoryRemove = new HashMap<>();
        memoryItems = new ArrayList<>();

        scoreboard.setText("Score: " + score);
        makeMemory();
        customHandler.postDelayed(updateTimerThread, 3000);


        gridView = (GridView) findViewById(R.id.gridView);
        Adapter = new gridviewAdapter(this, memoryItems);
        if(memoryItems.size() % 3 ==0) {
            gridView.setNumColumns((int) Math.round(memoryItems.size() / 3));
        }
        else {
            gridView.setNumColumns((int) Math.round(memoryItems.size() / 3) + 1);
        }
        gridView.setAdapter(Adapter);
        gridView.setOnItemClickListener(this);
        gridView.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    return true;
                }
                return false;
            }

        });
    }

    public void makeMemory() {
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("boos", false));
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("bang", false));
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("blij", false));
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("moe", false));
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("verbaasd", false));
        for (int i = 0; i < 2; i++)
            memoryItems.add(new MemoryItem("verdrietig", false));

        Collections.shuffle(memoryItems);
        for (int i = 0; i < memoryItems.size(); i++) {
            for (int ii = 0; ii < memoryItems.size(); ii++) {
                if (memoryItems.get(i).getImageview().equals(memoryItems.get(ii).getImageview()) && i != ii) {
                    if(!memory.containsValue(i)) {
                        memory.put(i, ii);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        memoryItems.get(position).setVisible(true);
        Adapter.notifyDataSetChanged();
        if(clickCounter < 3) {
            clickCounter++;
            if (match != -1) {
                if (position == match) {
                    for (Map.Entry<Integer, Integer> e : memory.entrySet()) {
                        if (e.getKey().equals(position) || e.getValue().equals(position)) {
                            memoryRemove.put(e.getKey(), e.getValue());
                            match = -1;
                            clickCounter = 0;
                            score += 10;
                        }
                    }

                } else {
                    customHandler.postDelayed(updateTimerThread, 250);
                    match = -1;
                    if(score > 0)
                        score -= 1;
                }
            } else {
                customHandler.removeCallbacks(updateTimerThread);
                for (int i = 0; i < memoryItems.size(); i++) {
                    if (memoryItems.get(position).getImageview().equals(memoryItems.get(i).getImageview())) {
                        if (memory.containsKey(position))
                            match = memory.get(i);
                        else
                            match = i;
                        break;
                    }
                }

            }
            boolean removed = false;
            for (int i = 0; i < memory.size(); i++) {
                for (Map.Entry<Integer, Integer> e : memoryRemove.entrySet()) {
                    if (memory.containsKey(e.getKey())) {
                        memory.remove(e.getKey());
                        removed = true;
                        break;
                    }
                }
                if (removed)
                    break;
            }

            if(memory.size() == 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Wilt u nogmaals spelen");

                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        memoryItems.clear();
                        score = 0;
                        scoreboard.setText("Score: " + score);
                        makeMemory();
                        Adapter.notifyDataSetChanged();
                    }
                });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            memoryRemove.clear();
        }
        else{
            customHandler.postDelayed(updateTimerThread, 0);
        }
        scoreboard.setText("Score: " + score);
    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            clickCounter = 0;
            for(int ii = 0; ii<memoryItems.size(); ii++) {
                if(memory.containsKey(ii)) {
                    memoryItems.get(ii).setVisible(false);
                    Adapter.notifyDataSetChanged();
                }
                else if(memory.containsValue(ii)) {
                    memoryItems.get(ii).setVisible(false);
                    Adapter.notifyDataSetChanged();
                }
            }
            Adapter.notifyDataSetChanged();
        }
    };
}
