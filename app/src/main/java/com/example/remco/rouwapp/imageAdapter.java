package com.example.remco.rouwapp;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Remco on 6-9-2016.
 */
public class imageAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<Person> persons = new ArrayList<Person>();

    public imageAdapter(Context context, ArrayList<Person>  person){
        this.context = context;
        this.persons = person;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int position) {
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View holder = mInflator.inflate(R.layout.listview_item,parent,false);

        TextView t = (TextView)holder.findViewById(R.id.Name);
        ImageView photo = (ImageView)holder.findViewById(R.id.photo);

        Person p = persons.get(position);
        if(p.getFirstName().equals(context.getResources().getString(R.string.New)) && p.getLastName().equals(context.getResources().getString(R.string.person))){
            photo.setImageDrawable(context.getResources().getDrawable(R.drawable.add_icon));
        }
        else {
            photo.setImageBitmap(StringToBitMap(p.getProfilphoto()));
            if (StringToBitMap(p.getProfilphoto()) == null)
                Picasso.with(context).load(p.getProfilphoto()).into(photo);
        }
        t.setText(p.getFirstName() +" "+ p.getLastName());
        return holder;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}
