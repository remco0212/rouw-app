package com.example.remco.rouwapp.View;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.remco.rouwapp.Feeling;
import com.example.remco.rouwapp.FeelingsDBHandler;
import com.example.remco.rouwapp.Kleurplaat;
import com.example.remco.rouwapp.KleurplaatAdapter;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.PersonCompare;
import com.example.remco.rouwapp.PersonDBHandler;
import com.example.remco.rouwapp.R;
import com.example.remco.rouwapp.Remember;
import com.example.remco.rouwapp.RememberAdapter;
import com.example.remco.rouwapp.RememberDBHandler;
import com.example.remco.rouwapp.feelingAdapter;
import com.example.remco.rouwapp.imageAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class ListviewActivity extends AppCompatActivity {

    ListView listView;
    Context context;
    imageAdapter arrayAdapter;
    com.example.remco.rouwapp.feelingAdapter feelingAdapter;
    com.example.remco.rouwapp.RememberAdapter rememberAdapter;
    ArrayList<Person> mPersonlist = new ArrayList<Person>();
    ArrayList<Feeling> mFeelinglist = new ArrayList<Feeling>();
    ArrayList<Kleurplaat> mKleurplaatlist = new ArrayList<Kleurplaat>();
    ArrayList<Remember> mRemberlist = new ArrayList<Remember>();
    PersonDBHandler pdb;
    FeelingsDBHandler fdb;
    RememberDBHandler rdb;
    private String userId = "";
    private Person p;
    private Feeling feeling1;
    private Remember remember;
    private String fromActivity = "";
    private KleurplaatAdapter kleurplaatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        listView = (ListView) findViewById(R.id.listView);

        pdb = new PersonDBHandler(getApplicationContext());
        fdb = new FeelingsDBHandler(getApplicationContext());
        rdb = new RememberDBHandler(getApplicationContext());

        Cursor cursor = pdb.getPersons();
        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            Person p = new Person();
            p.setUserID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id"))));
            p.setFirstName(cursor.getString(cursor.getColumnIndex("firstName")));
            p.setLastName(cursor.getString(cursor.getColumnIndex("lastName")));
            p.setAge(cursor.getString(cursor.getColumnIndex("age")));
            p.setEmail(cursor.getString(cursor.getColumnIndex("email")));
            p.setProfilphoto(cursor.getString(cursor.getColumnIndex("imageUrl")));
            p.setUsername(cursor.getString(cursor.getColumnIndex("username")));
            p.setGender(cursor.getString(cursor.getColumnIndex("gender")));
            p.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            p.setCity(cursor.getString(cursor.getColumnIndex("city")));
            mPersonlist.add(p);
        }

        try {
            Bundle bundle = getIntent().getExtras();
            p = (Person) getIntent().getExtras().getSerializable("Person");
            userId = p.getUserID() +"";
            fromActivity = (String) getIntent().getExtras().getSerializable("From");
            if(fromActivity == null)
                fromActivity ="";
            if(fromActivity.equals("gevoel")) {
                feeling1 = (Feeling) getIntent().getExtras().getSerializable("Feeling");
                fdb.addFeeling(feeling1);
            }
            if(fromActivity.equals("herinnering")) {
                remember = (Remember) getIntent().getExtras().getSerializable("Remember");
                rdb.addReminding(remember);
            }

        }
        catch (Exception e){}

        if(fromActivity == "") {
            arrayAdapter = new imageAdapter(this, mPersonlist);
            arrayAdapter.notifyDataSetChanged();
            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("selected", position + "");
                    if(position !=0) {
                        Person selectedRecipe = mPersonlist.get(position);
                        Intent detailIntent = new Intent(context, MenuActivity.class);
                        detailIntent.putExtra("Person", selectedRecipe);
                        startActivity(detailIntent);
                    }
                    else
                        startActivity( new Intent(context, NewpersonActivity.class));
                }
            });
            Collections.sort(mPersonlist, new PersonCompare());
            AddNewPerson();
        }else if(fromActivity.equals("herinneringen")||fromActivity.equals("herinnering")){
            Cursor cursorRemember = rdb.getRemindings(Integer.parseInt(userId));
            for(int i = 0;i<cursorRemember.getCount();i++){
                cursorRemember.moveToPosition(i);
                int userID = Integer.parseInt(cursorRemember.getString(cursorRemember.getColumnIndex("userID")));
                String name = cursorRemember.getString(cursorRemember.getColumnIndex("name"));
                String image = cursorRemember.getString(cursorRemember.getColumnIndex("image"));
                String hobby = cursorRemember.getString(cursorRemember.getColumnIndex("hobby"));
                String color = cursorRemember.getString(cursorRemember.getColumnIndex("color"));
                String happy = cursorRemember.getString(cursorRemember.getColumnIndex("happy"));
                String watch = cursorRemember.getString(cursorRemember.getColumnIndex("watch"));
                String animal = cursorRemember.getString(cursorRemember.getColumnIndex("animal"));
                String funny = cursorRemember.getString(cursorRemember.getColumnIndex("funny"));
                String humor = cursorRemember.getString(cursorRemember.getColumnIndex("humor"));
                String bijzonder = cursorRemember.getString(cursorRemember.getColumnIndex("bijzonder"));
                String together = cursorRemember.getString(cursorRemember.getColumnIndex("together"));
                String toSay = cursorRemember.getString(cursorRemember.getColumnIndex("toSay"));
                String thinkOf = cursorRemember.getString(cursorRemember.getColumnIndex("thinkOf"));
                String stays = cursorRemember.getString(cursorRemember.getColumnIndex("stays"));
                Remember rememberDB = new Remember(userID,name,image,hobby,color,happy,watch,animal,funny,humor,bijzonder,together,toSay,thinkOf,stays);

                mRemberlist.add(rememberDB);
            }

            TextView textView = (TextView)findViewById(R.id.textView2);
            textView.setText(getResources().getString(R.string.remindings));
            rememberAdapter = new RememberAdapter(this, mRemberlist);
            rememberAdapter.notifyDataSetChanged();
            listView.setAdapter(rememberAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent detailIntent = new Intent(context, RememberActivity.class);
                        detailIntent.putExtra("Id", userId +"");
                        detailIntent.putExtra("Person", p);
                        detailIntent.putExtra("Remember", mRemberlist.get(position));
                        startActivity(detailIntent);
                }
            });
            AddNewRemember();
        }
        else if(fromActivity.equals("kleurplaat")){
            TextView textView = (TextView)findViewById(R.id.textView2);
            textView.setText(getResources().getString(R.string.colouring));
            addKleurplaten();

            kleurplaatAdapter = new KleurplaatAdapter(this, mKleurplaatlist);
            kleurplaatAdapter.notifyDataSetChanged();
            listView.setAdapter(kleurplaatAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Kleurplaat kleurplaat = mKleurplaatlist.get(position);
                        Intent detailIntent = new Intent(context, DrawingActivity.class);
                        detailIntent.putExtra("KleurplaatID", kleurplaat);
                        startActivity(detailIntent);
                }
            });
        }
        else if(fromActivity.equals(getResources().getString(R.string.Feeling))){
            Cursor cursorFeeling = fdb.getFeelings(Integer.parseInt(userId));
            for(int i = 0;i<cursorFeeling.getCount();i++){
                cursorFeeling.moveToPosition(i);
                int userID = Integer.parseInt(cursorFeeling.getString(cursorFeeling.getColumnIndex("userId")));
                int id1 = Integer.parseInt(cursorFeeling.getString(cursorFeeling.getColumnIndex("id")));
                String date = cursorFeeling.getString(cursorFeeling.getColumnIndex("data"));
                Feeling feeling = new Feeling(userID,id1,date);

                mFeelinglist.add(feeling);
            }

            TextView textView = (TextView)findViewById(R.id.textView2);
            textView.setText(getResources().getString(R.string.New) + " "+getResources().getString(R.string.Feeling));
            feelingAdapter = new feelingAdapter(this, mFeelinglist);
            feelingAdapter.notifyDataSetChanged();
            listView.setAdapter(feelingAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(position !=0) {
                        Feeling feeling = mFeelinglist.get(position);
                        Intent detailIntent = new Intent(context, FeelingActivity.class);
                        detailIntent.putExtra("Feeling", feeling);
                        startActivity(detailIntent);
                    }
                    else{
                        Intent detailIntent = new Intent(context, NewfeelingActivity.class);
                        detailIntent.putExtra("Id", userId +"");
                        detailIntent.putExtra("Person", p);
                        startActivity(detailIntent);
                    }
                }
            });
            AddNewFeeling();
        }

        context = this;
    }

    private void AddNewPerson(){
        Person p1 = new Person();
        p1.setFirstName(getResources().getString(R.string.New));
        p1.setLastName(getResources().getString(R.string.person));
        mPersonlist.add(0,p1);
    }

    @Override
    public void onBackPressed() {
        if(!fromActivity.equals("")){
            Intent detailIntent = new Intent(context, MenuActivity.class);
            detailIntent.putExtra("Person", p);
            startActivity(detailIntent);
        }
        else{
            finish();
        }
    }

    private void addKleurplaten(){
        mKleurplaatlist.add(new Kleurplaat("bloemetje", "Bloemen","http://bestekleurplaten.nl/kleurplaten-van-bloemen/"));
        mKleurplaatlist.add(new Kleurplaat("knorretje", "Knorretje","http://www.kids-n-fun.nl/kleurplaten/winnie-de-pooh-en-knorretje"));
        mKleurplaatlist.add(new Kleurplaat("beertje","Beertje","http://www.kleurplaten.nl/valentijnsdag/--valentijnsdag-beertjes-k-9885.html"));
        mKleurplaatlist.add(new Kleurplaat("olifant","Olifant","http://www.kleurplaten.nl/olifant/schattige-olifant-olifantje-k-10271.html"));
        mKleurplaatlist.add(new Kleurplaat("kleurplaat1","Lego ninja","https://www.google.nl/search?q=kleurplaten&espv=2&biw=1920&bih=974&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiB_onvs4_QAhUMBMAKHY3DCSgQ_AUIBigB#tbm=isch&q=kleurplaten+lego+ninja&imgrc=GwgAKNuIp95mdM%3A"));
        mKleurplaatlist.add(new Kleurplaat("vlinder", "Vlinder","http://www.kleurplaten-voor-kids.nl/index.php?option=com_kleurplaatprinting&img=5838&fld=/Dieren/Vlinders/page2.html"));
    }
    private void AddNewFeeling(){
        Feeling feeling = new Feeling(p.userID,8,getResources().getString(R.string.New) + " "+ getResources().getString(R.string.Feeling));
        mFeelinglist.add(0,feeling);
    }
    private void AddNewRemember(){
        Remember remember = new Remember(p.userID,getResources().getString(R.string.Newe) + " "+ getResources().getString(R.string.reminding),"","", "","","","","","","","","","","");
        mRemberlist.add(0,remember);
    }
}
