package com.example.remco.rouwapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.Serializable;

/**
 * Created by Remco on 10-10-2016.
 */
public class FeelingsDBHandler extends SQLiteAssetHelper implements Serializable {

    private static final String TAG = "FeelingDBHandler";

    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "feeling.db";
    private static final String DB_TABLE_NAME = "feelings";

    // Tabel en kolom namen ...
    private static final String COLOMN_userID = "userId";
    private static final String COLOMN_ID = "id";
    private static final String COLOMN_DATE = "data";


    public FeelingsDBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Hier de CRUD methoden
    public Cursor getFeelings(int userid) {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + DB_TABLE_NAME +" WHERE userId=" + userid;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        db.close();
        System.out.println("Count: " + c.getCount());
        return c;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        db.execSQL("CREATE TABLE \"feelings\" ( `userId` INTEGER, `id` INTEGER, `data` TEXT )");
    }

    public void addFeeling(Feeling feeling)
    {
        ContentValues values = new ContentValues();
        values.put(COLOMN_userID, feeling.getUserId());
        values.put(COLOMN_ID, feeling.getId());
        values.put(COLOMN_DATE, feeling.getDate().toString());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DB_TABLE_NAME, null, values);
        db.close();
    }
}
