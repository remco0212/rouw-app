package com.example.remco.rouwapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.Serializable;

/**
 * Created by Remco on 27-10-2016.
 */
public class RememberDBHandler extends SQLiteAssetHelper implements Serializable {

    private static final String TAG = "RememberDBHandler";

    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "Remindings.db";
    private static final String DB_TABLE_NAME = "remindings";

    // Tabel en kolom namen ...
    private static final String COLOMN_userID = "userID";
    private static final String COLOMN_name = "name";
    private static final String COLOMN_hobby = "hobby";
    private static final String COLOMN_image = "image";
    private static final String COLOMN_color = "color";
    private static final String COLOMN_happy =  "happy";
    private static final String COLOMN_watch =  "watch";
    private static final String COLOMN_animal =  "animal";
    private static final String COLOMN_funny =  "funny";
    private static final String COLOMN_humor =  "humor";
    private static final String COLOMN_bijzonder =  "bijzonder";
    private static final String COLOMN_together =  "together";
    private static final String COLOMN_toSay =  "toSay";
    private static final String COLOMN_thinkOf =  "thinkOf";
    private static final String COLOMN_stays =  "stays";


    public RememberDBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Hier de CRUD methoden
    public Cursor getRemindings(int userid) {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + DB_TABLE_NAME +" WHERE userID=" + userid;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        db.close();
        System.out.println("Count: " + c.getCount());
        return c;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        db.execSQL("CREATE TABLE `remindings` (\n" +
                "\t`userID`\tINTEGER,\n" +
                "\t`name`\tTEXT,\n" +
                "\t`image`\tTEXT,\n" +
                "\t`hobby`\tTEXT,\n" +
                "\t`color`\tTEXT,\n" +
                "\t`happy`\tTEXT,\n" +
                "\t`watch`\tTEXT,\n" +
                "\t`animal`\tTEXT,\n" +
                "\t`funny`\tTEXT,\n" +
                "\t`humor`\tTEXT,\n" +
                "\t`bijzonder`\tTEXT,\n" +
                "\t`together`\tTEXT,\n" +
                "\t`toSay`\tTEXT,\n" +
                "\t`thinkOf`\tTEXT,\n" +
                "\t`stays`\tTEXT\n" +
                "))");
    }

    public void addReminding(Remember remember)
    {
        ContentValues values = new ContentValues();
        values.put(COLOMN_userID, remember.getUserId());
        values.put(COLOMN_name, remember.getName());
        values.put(COLOMN_hobby, remember.getHobby());
        values.put(COLOMN_image, remember.getImage());
        values.put(COLOMN_color, remember.getColor());
        values.put(COLOMN_happy, remember.getHappy());
        values.put(COLOMN_watch, remember.getWatch());
        values.put(COLOMN_animal, remember.getAnimal());
        values.put(COLOMN_funny, remember.getFunny());
        values.put(COLOMN_humor, remember.getHumor());
        values.put(COLOMN_bijzonder, remember.getBijzonder());
        values.put(COLOMN_together, remember.getTogether());
        values.put(COLOMN_toSay, remember.getToSay());
        values.put(COLOMN_thinkOf, remember.getThinkOf());
        values.put(COLOMN_stays, remember.getStays());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DB_TABLE_NAME, null, values);
        db.close();
    }
}
