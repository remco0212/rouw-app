package com.example.remco.rouwapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class PersonDBHandler extends SQLiteAssetHelper {

    private static final String TAG = "PersonDBHandler";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "person.db";
    private static final String DB_TABLE_NAME = "persons";

    // Tabel en kolom namen ...
    private static final String COLOMN_ID = "_id";  // primary key, auto increment
    private static final String COLOMN_FIRSTNAME = "firstName";
    private static final String COLOMN_LASTNAME = "lastName";
    private static final String COLOMN_EMAIL = "email";
    private static final String COLOMN_AGE = "age";
    private static final String COLOMN_IMAGEURL = "imageUrl";
    private static final String COLOMN_USERNAME = "username";
    private static final String COLOMN_CITY = "city";
    private static final String COLOMN_TITLE = "title";
    private static final String COLOMN_GENDER = "gender";


    public PersonDBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Hier de CRUD methoden
    public Cursor getPersons() {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + DB_TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        db.close();
        System.out.println("Count: " + c.getCount());
        return c;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        db.execSQL("CREATE TABLE `persons` ( `_id` INTEGER, `firstName` TEXT, `lastName` TEXT, `email` TEXT, `age` TEXT, `imageUrl` TEXT, `gender` TEXT, `title` TEXT, `city` TEXT, `username` TEXT, PRIMARY KEY(`_id`) )");
    }
    public void addPerson(Person person)
    {
        ContentValues values = new ContentValues();
        values.put(COLOMN_FIRSTNAME, person.getFirstName());
        values.put(COLOMN_LASTNAME, person.getLastName());
        values.put(COLOMN_EMAIL, person.getEmail());
        values.put(COLOMN_AGE, person.getAge());
        values.put(COLOMN_IMAGEURL, person.getProfilphoto());
        values.put(COLOMN_GENDER, person.getGender());
        values.put(COLOMN_TITLE, person.getTitle());
        values.put(COLOMN_CITY, person.getCity());
        values.put(COLOMN_USERNAME, person.getUsername());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DB_TABLE_NAME, null, values);
        db.close();
    }
}
