package com.example.remco.rouwapp.View;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.remco.rouwapp.Feeling;
import com.example.remco.rouwapp.R;

/**
 * Created by Remco on 10-10-2016.
 */
public class FeelingActivity extends AppCompatActivity{

    Feeling feeling;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feelings);

        Bundle bundle = getIntent().getExtras();
        feeling = (Feeling) getIntent().getExtras().getSerializable("Feeling");

        TextView date = (TextView)findViewById(R.id.textView7);
        TextView feelingName = (TextView)findViewById(R.id.textView8);
        ImageView photo = (ImageView)findViewById(R.id.imageView3);

        date.setText(feeling.getDate());
        feelingID(feeling.getId(),photo,feelingName);
    }

    private void feelingID(int id, ImageView imageView,TextView Text){
        String name = "";
        switch(id){
            case 0:
                name ="Blij";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.blij)));
                break;
            case 1:
                name ="Boos";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.boos)));
                break;
            case 2:
                name ="Verdrietig";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.verdrietig)));
                break;
            case 3:
                name ="Moe";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.moe)));
                break;
            case 4:
                name ="Bang";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.bang)));
                break;
            case 5:
                name ="Verbaasd";
                imageView.setImageBitmap(resizeImage(getResources().getDrawable(R.drawable.verbaasd)));
                break;
        }
        Text.setText(name);
    }

    private Bitmap resizeImage(Drawable image){
        Bitmap b = ((BitmapDrawable) image ).getBitmap();
        return Bitmap.createScaledBitmap(b, image.getMinimumWidth() * 2, image.getMinimumHeight()*2, false);
    }
}
