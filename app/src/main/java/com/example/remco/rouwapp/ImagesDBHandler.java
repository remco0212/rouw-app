package com.example.remco.rouwapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.Serializable;

/**
 * Created by Remco on 12-10-2016.
 */
public class ImagesDBHandler extends SQLiteAssetHelper implements Serializable {

    private static final String TAG = "ImageDBHandler";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "Images.db";
    private static final String DB_TABLE_NAME = "Images";

    // Tabel en kolom namen ...
    private static final String COLOMN_userID = "userId";
    private static final String COLOMN_IMAGE = "image";


    public ImagesDBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Hier de CRUD methoden
    public Cursor getImages(int userid) {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + DB_TABLE_NAME +" WHERE userId=" + userid;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        db.close();
        System.out.println("Count: " + c.getCount());
        return c;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        db.execSQL("CREATE TABLE `Images` ( `userId` INTEGER, `image` TEXT )");
    }

    public void addImage(int userId, String image)
    {
        ContentValues values = new ContentValues();
        values.put(COLOMN_userID, userId);
        values.put(COLOMN_IMAGE,image);

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DB_TABLE_NAME, null, values);
        db.close();
    }

    public void removeImage(int userId, String image)
    {
        String DeleteQuery = "DELETE FROM "+DB_TABLE_NAME+" WHERE userId=" + userId + " AND image=\"" +image +"\";";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DeleteQuery);
    }
}
