package com.example.remco.rouwapp.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.remco.rouwapp.R;

/**
 * Created by Remco on 10-10-2016.
 */
public class ParentActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);


        Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(this);
        Button button1 = (Button) findViewById(R.id.button3);
        button1.setOnClickListener(this);
        Button button2 = (Button) findViewById(R.id.button4);
        button2.setOnClickListener(this);
        Button button3 = (Button) findViewById(R.id.button5);
        button3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button2:
                Intent detailIntent1 = new Intent(this, TextviewActivity.class);
                detailIntent1.putExtra("id", "button2");
                startActivity(detailIntent1);
                break;
            case R.id.button3:
                Intent detailIntent2 = new Intent(this, TextviewActivity.class);
                detailIntent2.putExtra("id", "button3");
                startActivity(detailIntent2);
                break;
            case R.id.button4:
                Intent detailIntent3 = new Intent(this, TextviewActivity.class);
                detailIntent3.putExtra("id", "button4");
                startActivity(detailIntent3);
                break;
            case R.id.button5:
                Intent detailIntent4 = new Intent(this, TextviewActivity.class);
                detailIntent4.putExtra("id", "button5");
                startActivity(detailIntent4);
                break;
        }
    }
}
