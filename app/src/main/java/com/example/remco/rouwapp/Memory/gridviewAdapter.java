package com.example.remco.rouwapp.Memory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.remco.rouwapp.R;

import java.util.ArrayList;

/**
 * Created by Remco on 17-10-2016.
 */
public class gridviewAdapter extends BaseAdapter {
    private Context context;
    ArrayList<MemoryItem> MemoryItems = new ArrayList<MemoryItem>();

    public gridviewAdapter(Context context, ArrayList<MemoryItem> MemoryItems) {
        this.context = context;
        this.MemoryItems = MemoryItems;
    }

    @Override
    public int getCount() {
        return MemoryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return MemoryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =
                (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if(convertView == null) {
            gridView = new View(this.context);
            gridView = inflater.inflate(R.layout.memory_item, null);
        } else {
            gridView = (View) convertView;
        }
        ImageView imageView = (ImageView) gridView.findViewById(R.id.imageView);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        if(MemoryItems.get(position).getVisible()) {
            Drawable drawable = context.getResources().getDrawable(context.getResources().getIdentifier(MemoryItems.get(position).getImageview(), "drawable", context.getPackageName()));
            Bitmap b = ((BitmapDrawable)drawable).getBitmap();
            imageView.setImageBitmap(Bitmap.createScaledBitmap(b, (size.y-40)/(MemoryItems.size()/3), (size.y-(size.y/5))/(MemoryItems.size()/3), false));
        }
            else
            imageView.setImageResource(R.color.headerColor);

        imageView.setMinimumWidth((size.y-40)/(MemoryItems.size()/3));
        imageView.setMinimumHeight((size.y-(size.y/5))/(MemoryItems.size()/3));
        return gridView;
    }
}