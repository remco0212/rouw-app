package com.example.remco.rouwapp.View;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.PersonDBHandler;
import com.example.remco.rouwapp.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Remco on 10-10-2016.
 */
public class NewpersonActivity extends AppCompatActivity implements View.OnClickListener {
    PersonDBHandler pdb;
    Person selectedRecipe = new Person();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newperson);
        pdb = new PersonDBHandler(this);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        Button imageButton = (Button) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         Bitmap bm= null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ImageView view = (ImageView)findViewById(R.id.profilePhoto);
                view.setImageBitmap(overlay(bm,BitmapFactory.decodeResource(this.getResources(), R.drawable.fotolijst)));
                //view.setImageBitmap(bm);
                selectedRecipe.setProfilphoto(BitMapToString(bm));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp2.getWidth(), bmp2.getHeight(), bmp2.getConfig());
        Canvas canvas = new Canvas(bmOverlay);

        canvas.drawBitmap(getResizedBitmap(bmp1,bmp2.getWidth(),bmp2.getHeight()), new Matrix(), null);
        canvas.drawBitmap(bmp2, new Matrix(), null);
        return bmOverlay;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        //bm.recycle();
        return resizedBitmap;
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageButton:
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                break;
            case R.id.button:
                EditText voornaam = (EditText) findViewById(R.id.editText3);
                EditText achternaam = (EditText) findViewById(R.id.editText2);

                selectedRecipe.setFirstName(voornaam.getText().toString());
                selectedRecipe.setLastName(achternaam.getText().toString());
                pdb.addPerson(selectedRecipe);
                Intent detailIntent = new Intent(this, MenuActivity.class);
                detailIntent.putExtra("Person", selectedRecipe);
                startActivity(detailIntent);
                break;
            default:
                break;
        }
    }
}
