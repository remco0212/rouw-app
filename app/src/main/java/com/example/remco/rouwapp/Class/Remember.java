package com.example.remco.rouwapp;

import java.io.Serializable;

/**
 * Created by Remco on 27-10-2016.
 */
public class Remember implements Serializable {

    private int userId;
    private String name;
    private String image;
    private String hobby;
    private String color;
    private String happy;
    private String watch;
    private String animal;
    private String funny;
    private String humor;
    private String bijzonder;
    private String together;
    private String toSay;
    private String thinkOf;
    private String stays;

    public Remember(int userId, String name, String image, String hobby, String color, String happy, String watch, String animal, String funny, String humor, String bijzonder, String together, String toSay, String thinkOf, String stays) {
        this.userId = userId;
        this.name = name;
        this.image = image;
        this.hobby = hobby;
        this.color = color;
        this.happy = happy;
        this.watch = watch;
        this.animal = animal;
        this.funny = funny;
        this.humor = humor;
        this.bijzonder = bijzonder;
        this.together = together;
        this.toSay = toSay;
        this.thinkOf = thinkOf;
        this.stays = stays;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getColor() {
        return color;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHappy() {
        return happy;
    }

    public void setHappy(String happy) {
        this.happy = happy;
    }

    public String getWatch() {
        return watch;
    }

    public void setWatch(String watch) {
        this.watch = watch;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getFunny() {
        return funny;
    }

    public void setFunny(String funny) {
        this.funny = funny;
    }

    public String getHumor() {
        return humor;
    }

    public void setHumor(String humor) {
        this.humor = humor;
    }

    public String getBijzonder() {
        return bijzonder;
    }

    public void setBijzonder(String bijzonder) {
        this.bijzonder = bijzonder;
    }

    public String getTogether() {
        return together;
    }

    public void setTogether(String together) {
        this.together = together;
    }

    public String getToSay() {
        return toSay;
    }

    public void setToSay(String toSay) {
        this.toSay = toSay;
    }

    public String getThinkOf() {
        return thinkOf;
    }

    public void setThinkOf(String thinkOf) {
        this.thinkOf = thinkOf;
    }

    public String getStays() {
        return stays;
    }

    public void setStays(String stays) {
        this.stays = stays;
    }
}
