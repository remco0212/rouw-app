package com.example.remco.rouwapp.Class;

import java.io.Serializable;

/**
 * Created by Remco on 22-11-2016.
 */
public class Picture implements Serializable {

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Picture(byte[] image){
        this.image = image;
    }
    private byte[] image;


}
