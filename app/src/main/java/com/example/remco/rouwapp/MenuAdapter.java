package com.example.remco.rouwapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Remco on 10-10-2016.
 */
public class MenuAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<CustomMenuItem> menuItems = new ArrayList<CustomMenuItem>();

    public MenuAdapter(Context context, ArrayList<CustomMenuItem>  menuItems){
        this.context = context;
        this.menuItems = menuItems;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View holder;

        if(convertView == null) {
            holder = new View(this.context);
            holder = mInflator.inflate(R.layout.gridview_item,parent,false);
        } else {
            holder = (View) convertView;
        }
        TextView t = (TextView)holder.findViewById(R.id.textView);
        ImageView photo = (ImageView)holder.findViewById(R.id.imageView2);

        CustomMenuItem menuItem = menuItems.get(position);
        if(menuItem.isFeeling()) {
            setFeelingImage(menuItem.getId(), photo);
        }
        else {
            setMenuImage(menuItem.getId(), photo);
        }
        t.setText(menuItem.getName());
        return holder;
    }

    public void setFeelingImage(int id, ImageView imageView){
        switch(id){
            case 0:
                imageView.setImageResource(R.drawable.blij);
                break;
            case 1:
                imageView.setImageResource(R.drawable.boos);
                break;
            case 2:
                imageView.setImageResource(R.drawable.verdrietig);
                break;
            case 3:
                imageView.setImageResource(R.drawable.moe);
                break;
            case 4:
                imageView.setImageResource(R.drawable.bang);
                break;
            case 5:
                imageView.setImageResource(R.drawable.verbaasd);
                break;
            case 8:
                imageView.setImageResource(R.drawable.add_icon);
                break;
        }
    }

    public void setMenuImage(int id, ImageView imageView){
        switch(id){
            case 0:
                imageView.setImageResource(R.drawable.mijngevoel);
                break;
            case 1:
                imageView.setImageResource(R.drawable.kleurplatenmenu);
                break;
            case 2:
                imageView.setImageResource(R.drawable.herinneringen);
                break;
            case 3:
                imageView.setImageResource(R.drawable.emoties);
                break;
            case 4:
                imageView.setImageResource(R.drawable.ouders);
                break;
            case 5:
                imageView.setImageResource(R.drawable.afbeeldingen);
                break;
        }
    }
}
