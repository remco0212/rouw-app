package com.example.remco.rouwapp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Remco on 10-10-2016.
 */
public class Feeling implements Serializable {
    private int id;
    private int userId;
    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Feeling(int userId, int id, String date){
        this.userId = userId;
        this.id = id;
        this.date = date;
    }
}
