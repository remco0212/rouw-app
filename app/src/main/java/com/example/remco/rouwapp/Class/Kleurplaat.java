package com.example.remco.rouwapp;

import java.io.Serializable;

/**
 * Created by Remco on 12-10-2016.
 */
public class Kleurplaat implements Serializable {

    private String kleurplaatID;
    private String name;
    private String sourceKleurplaat;
    public Kleurplaat(String kleurplaatID, String name,String sourceKleurplaat) {
        this.kleurplaatID = kleurplaatID;
        this.name = name;
        this.sourceKleurplaat = sourceKleurplaat;
    }

    public String getKleurplaatID() {
        return kleurplaatID;
    }

    public void setKleurplaatID(String kleurplaatID) {
        this.kleurplaatID = kleurplaatID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceKleurplaat() {
        return sourceKleurplaat;
    }

    public void setSourceKleurplaat(String sourceKleurplaat) {
        this.sourceKleurplaat = sourceKleurplaat;
    }
}
