package com.example.remco.rouwapp.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.remco.rouwapp.R;

/**
 * Created by Remco on 10-10-2016.
 */
public class TextviewActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_textview);

        Bundle bundle = getIntent().getExtras();
        String id = (String) getIntent().getExtras().getSerializable("id");

        TextView text = (TextView) findViewById(R.id.textView6);
        switch (id){
            case "button2":
                text.setText("Rouw is het geheel van lichamelijke, emotionele, cognitieve en gedragsmatige reacties die optreden wanneer een dierbaar persoon is overleden. Tijdens het rouwproces kunnen er verschillende reacties optreden \n" +
                        "\n-\tLichamelijke reacties: slaapproblemen, verminderde eetlust, spanningshoofdpijn;\n" +
                        "\n-\tEmotionele reacties: verdriet, eenzaamheid, angst, schuldgevoelens, agressie, machteloosheid, opluchting\n" +
                        "\n-\tCognitieve reacties: concentratieverlies, verminderde zelfwaardering, verwardheid, gespannenheid, hopeloosheid\n" +
                        "\n-\tGedragsmatige reacties: gespannenheid, teruggetrokkenheid, zoekgedrag, vermijding van personen of situaties (NJI, 2015). \n");
                break;
            case "button3":
                text.setText("Kinderen rouwen op hun eigen manier. Het rouwproces van kinderen lijkt op het rouwproces van volwassenen, want zowel volwassenen als kinderen kennen gevoelens als verdriet, boosheid, angst of schuldgevoelens. Tijdens het rouwproces kunnen kinderen de volgende emoties ervaren:\n" +
                        "\n-\tAngst en zorgen: kinderen kunnen de angst ontwikkelen dat ze zelf of een andere dierbare ook dood gaan. \n" +
                        "-\tOngeloof: Kinderen ervaren dit op het moment dat ze niet zelf hebben kunnen zien dat de persoon is overleden.\n" +
                        "-\tBoosheid: woede en agressie horen bij het rouwproces van het kind. Het kind kan boos worden, omdat het beseft dat de overleden persoon niet meer terug komt. \n" +
                        "-\tSchuldgevoel\n" +
                        "-\tLichamelijke klachten: kinderen kunnen hun gevoelens nog niet onder woorden brengen en daarom komen hun emoties soms tot uiting in lichamelijke klachten zoals hoesten of buikpijn.\n" +
                        "-\tJaloezie: kinderen kunnen jaloers zijn op kinderen die nog wel alle dierbare personen om hun heen hebben, bijv. nog alle opa’s en oma’s (Stichtingsterrekracht). \n" +
                        "\n" +
                        "Wat het verschil maakt tussen het rouwproces van volwassenen en dat van kinderen is het feit dat kinderen deze gevoelens vooral non-verbaal uiten (achterderegenboog, 2014).Kinderen laten namelijk in hun gedrag zien dat ze rouwen en dat ze dus verdrietig zijn. Maar dat gedrag is niet altijd te herkennen. Soms uit het verdriet van een kind zich in agressief gedraag en soms uit het verdriet van een kind zich doordat het kind zich terugtrekt en op de achtergrond blijft. Naast dat het verdriet van het kind zich kan uit in agressief gedrag en het op de achtergrond blijven, kan het kind ook veel vragen gaan stellen. Vaak zijn deze vragen heel direct en confronterend voor ouders. Het is goed om alle vragen van het kind duidelijk te beantwoorden en het kind gerust te stellen (uitvaart.com, 2016).\n" +
                        "\n" +
                        "Kinderen laten hun emoties niet alleen zien in hun gedrag, maar laten hun emoties ook zien in hun spel. Ze spelen de gebeurtenis na door bijvoorbeeld poppen in een schoenendoos te doen of een verhaal te vertellen met een poppenkast (Fiddelaers-Jaspers, 2006). \n" +
                        "\n" +
                        "Tijdens het rouwproces van het kind kan het kind verschillende dingen ervaren. Hoe het kind omgaat met het verlies is vaak te zien aan het gedrag van het kind. De volgende ervaringen/symptomen kunnen tijdens het rouwproces aan bod komen:\n\n" +
                        "\n-\tDenken dat de overledene terugkomt;\n" +
                        "-\tRegressief gedrag: dat betekent dat het kind gedrag vertoont dat niet meer bij de leeftijd van het kind past (bijv. duimzuigen);\n" +
                        "-\tTerugtrekken;\n" +
                        "-\tConfronterende opmerkingen maken: de opmerkingen die kinderen maken zijn voor volwassenen vaak erg confronterend;\n" +
                        "-\tVerdriet uitstellen;\n" +
                        "-\tVragen stellen;\n" +
                        "-\tOntkennen wat er is gebeurd;\n" +
                        "-\tConcentratieproblemen (Stichtingsterrekracht). \n");
                break;
            case "button4":
                text.setText("Kinderen krijgen te maken met heel wat emoties. Het is dan ook belangrijk dat kinderen hun emoties voelen, kunnen benoemen en kunnen uiten. Volwassenen hebben de taak om kinderen dit te leren. Vooral de ouders van het kind spelen een belangrijke rol in het leren voelen, benoemen en uiten van emotie (Bieger, 2012). \n" +
                        "\n" +
                        "Wat zijn emoties?\n" +
                        "Het woord emoties is af te leiden van het Latijnse woord emovere wat betekent ‘doen bewegen’ of ‘ergens uitdrijven’. Emoties zorgen ervoor dat we in beweging komen met ons lichaam of met onze gedachten. Emoties helpen ons om met anderen te communiceren en dragen bij aan de ontwikkeling en het welzijn van kinderen. Een kind dat zijn emoties niet meer wil voelen ontwikkeld een negatief zelfbeeld en voelt zich vaak ongelukkig (Bieger, 2012). \n" +
                        "\n" +
                        "Je kan een gevoel pas uiten op het moment dat je het voelt. Als volwassenen kan je letterlijk aan een kind vragen of hij zijn hand op het gevoel wil leggen. Een kind kan vaak heel goed benoemen wat het voelt. Wanneer het kind verschillende emoties kan voelen is het van belang dat het kind de verschillende emoties leert herkennen. Het kind leert dit doordat de volwassenen vraagt hoe het voelt om blij te zijn, hoe het voelt  om heel verdrietig zijn en hoe voelt het om boos te zijn. Doordat kinderen antwoord geven op deze vragen leren ze de verschillende emoties kennen en leren ze dat een emoties ook weer over gaat. Op het moment dat het kind de verschillende emoties herken is de volgende stap dat het kind de emoties gaat benoemen. Als volwassenen moet je het kind laten zeggen wat hij voelt en moet je zijn waarheid accepteren. Tenslotte is het erg belangrijk dat kinderen hun emoties ook uiten. Een kind moet leren dat emoties een hulpmiddel zijn om voor jezelf op te komen (Bieger, 2012).\n" +
                        "\n" +
                        "Kinderen genieten er van om te werken met emoties. Dit kan bijvoorbeeld in de vorm van toneelspel, lachen, muziek, opdrachten en tekenen. Het creatief omgaan met emoties is voor kinderen een manier om met bepaalde emoties om te leren gaan (Bieger, 2012).\n");
                break;
            case "button5":
                text.setText("Het is goed om eerlijk te zijn naar kinderen toe. Het is dan ook goed om kinderen te informeren over wat er gaat gebeuren of wat er gebeurd is. Door als volwassene duidelijkheid te bieden aan het kind, geef je het kid het gevoel dat het erbij hoort. Ook verstevigd de duidelijkheid het basisvertrouwen in volwassenen. Hoe je duidelijkheid en eerlijkheid biedt aan een kind is afhankelijk van de ontwikkelingsfase en de levenservaring van het kind. Een jong kind begrijpt het begrip dood nog niet helemaal. Maar als de opa of oma net is overleden, maakt deze ervaring het kind op het gebied van de dood een stuk wijzer (Dabekaussen, 2015).\n" +
                        "\n" +
                        "Rouw is niet is dat je kan verwerken met een eindpunt, maar is iets wat je in je leven moet verwerken. In Nederland werken we met het ‘rouwtakenmodel’. Het model bestaat uit de volgende taken:\n" +
                        "\n1)\tBeseffen dat iemand echt dood is. Dit gaat erom dat het kind erkent dat iemand nooit meer terug komt. Om het door te laten dringen dat iemand echt dood is kan het kinderen helpen om de overledene nog een keer te zien om echt afscheid te kunnen nemen. Ook is het belangrijk dat je als volwassene aandacht besteed aan de ervaringen van kinderen. Nodig het kind uit om op zijn eigen manier te vertellen over de gebeurtenis. Dit kan met of zonder woorden. Een tekening kan voor een kind al duidelijk maken wat er is gebeurd. Wanneer je aanleiding biedt erover te praten ervaren ze de realiteit steeds meer.\n" +
                        "\n2)\tHet voelen van pijn door het verlies. Bij het kind wisselen verschillende gevoelens zich af, waardoor er verwarring kan ontstaan bij het kind. Het helpt kinderen als ze deze gevoelens gaan herkennen en kunnen uiten. Door gevoelens te benomen en kinderen de ruimte te bieden om gevoelens te uiten help je het kind om zijn gevoelens te verwoorden of te uiten. Hierbij kan het helpen om de zogenaamde symbooltaal te gebruiken. Symbooltaal is een creatieve taal waarbij kinderen door middel van activiteiten en creatieve expressie de kans krijgen zich te uiten zonder daar woorden aan te hoeven geven.\n" +
                        "\n3)\tHet aanpassen aan de nieuwe situatie. Bij deze taak moeten kinderen gaan verkennen hoe ze nu verder moeten en hoe ze het leven weer op pakken en herinneringen een plaats te geven. In deze rouwtaak spelen herinneringen en rituelen een belangrijke rol. Kinderen hebben namelijk tastbare voorwerpen nodig om tot een verhaal te komen. Door samen herinneringen vast te leggen in bijvoorbeeld een herinneringsboek help je het kind om zijn verhaal te doen en dus te vertellen wat er in hem of haar om gaat. \n" +
                        "\n4)\tHet leven hervatten (Dabekaussen, 2015). \n");
                break;
        }
    }
}