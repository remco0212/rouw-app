package com.example.remco.rouwapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;

/**
 * Created by Remco on 12-10-2016.
 */
public class gridViewImageAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<Bitmap> images;

    public gridViewImageAdapter(Context context, ArrayList<Bitmap> images) {
        this.context = context;
        this.images = images;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View gridView;

        if(convertView == null) {
            gridView = new View(context);
            gridView = mInflator.inflate(R.layout.only_image, null);
        } else {
            gridView = (View) convertView;
        }

        ImageView photo = (ImageView) gridView.findViewById(R.id.imageView4);
        ProgressBar progressBar = (ProgressBar) gridView.findViewById(R.id.progressBar1);


        Bitmap image = images.get(position);

        if(image.getWidth() == 190)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);

        photo.setScaleType(ImageView.ScaleType.FIT_XY);
        photo.setImageBitmap(image);

        return gridView;
    }
}
