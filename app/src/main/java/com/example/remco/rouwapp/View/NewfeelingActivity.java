package com.example.remco.rouwapp.View;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.remco.rouwapp.CustomMenuItem;
import com.example.remco.rouwapp.Feeling;
import com.example.remco.rouwapp.MenuAdapter;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Remco on 10-10-2016.
 */
public class NewfeelingActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener{

    private GridView gridView;
    private Context context;
    private String userID;
    private ArrayList<CustomMenuItem> menuItems;
    private int id = 0;
    private Person person;
    private static View overalView;
    private MenuAdapter menuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feelings_new);

        Button button = (Button) findViewById(R.id.button6);
        button.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        userID = (String) getIntent().getExtras().getSerializable("Id");
        person = (Person) getIntent().getExtras().getSerializable("Person");

        menuItems = new ArrayList<>();
        genarteFeeling();
        gridView = (GridView) findViewById(R.id.gridView2);
        menuAdapter = new MenuAdapter(this, menuItems);
        gridView.setAdapter(menuAdapter);
        gridView.setOnItemClickListener(this);
        context = this;
    }

    private void genarteFeeling(){
        menuItems.add(new CustomMenuItem("Blij",0,true));
        menuItems.add(new CustomMenuItem("Boos",1,true));
        menuItems.add(new CustomMenuItem("Verdrietig",2,true));
        menuItems.add(new CustomMenuItem("Moe",3,true));
        menuItems.add(new CustomMenuItem("Bang",4,true));
        menuItems.add(new CustomMenuItem("Verbaasd",5,true));
    }

    @Override
    public void onClick(View v) {
        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String today = formatter.format(date);

        Feeling feeling = new Feeling(Integer.parseInt(userID),id,today);
        Intent detailIntent = new Intent(getApplicationContext(), ListviewActivity.class);
        detailIntent.putExtra("Person", person);
        detailIntent.putExtra("Feeling", feeling);
        detailIntent.putExtra("From", getResources().getString(R.string.Feeling));
        startActivity(detailIntent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        ImageView photo = (ImageView) view.findViewById(R.id.imageView2);
        if(this.id >= 0){
            menuItems.clear();
            genarteFeeling();
            menuAdapter.notifyDataSetChanged();
        }
        this.id = position;
        final int pos = position;
        final View viewFinal = view;
        overalView = view;
        gridView.post( new Runnable() {
            @Override
            public void run() {
                ImageView photo = (ImageView) viewFinal.findViewById(R.id.imageView2);
                setImage(pos,photo);
                photo.buildDrawingCache();
                photo.setImageBitmap(getCroppedBitmap(photo.getDrawingCache()));
            }
        });
    }

    public void setImage(int id, ImageView imageView){
        switch(id){
            case 0:
                imageView.setImageResource(R.drawable.blij);
                break;
            case 1:
                imageView.setImageResource(R.drawable.boos);
                break;
            case 2:
                imageView.setImageResource(R.drawable.verdrietig);
                break;
            case 3:
                imageView.setImageResource(R.drawable.moe);
                break;
            case 4:
                imageView.setImageResource(R.drawable.bang);
                break;
            case 5:
                imageView.setImageResource(R.drawable.verbaasd);
                break;
            case 8:
                imageView.setImageResource(R.drawable.add_icon);
                break;
        }
    }

    public static Bitmap getCroppedBitmap(Bitmap bmp) {
        Bitmap sbmp = bmp;

        Bitmap output = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();

        Rect rect = new Rect(0, 0,  overalView.findViewById(R.id.imageView2).getWidth(), overalView.findViewById(R.id.imageView2).getHeight());

        paint.setAntiAlias(true);

        paint.setFilterBitmap(true);

        paint.setDither(true);

        canvas.drawARGB(0, 0, 0, 0);

        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawRect(0,0,bmp.getWidth(),bmp.getHeight(),paint);
        canvas.drawBitmap(sbmp, rect, rect, paint);
        return output;
    }
}