package com.example.remco.rouwapp.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.remco.rouwapp.CustomMenuItem;
import com.example.remco.rouwapp.ImagesDBHandler;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.R;
import com.example.remco.rouwapp.gridViewImageAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Remco on 4-11-2016.
 */
public class PictureActivity extends AppCompatActivity {

    private Person p;
    private Context context;
    private LinearLayout linearLayout;
    private ArrayList<Bitmap> customImages;
    private Hashtable<Bitmap,String> path;
    private Intent intent;
    private ImagesDBHandler idbh;
    public gridViewImageAdapter viewImageAdapter;
    private GridView gridView;
    private FloatingActionButton fabButtonDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_pictures);

        Bundle bundle = getIntent().getExtras();
        p = (Person) getIntent().getExtras().getSerializable("Person");

        idbh = new ImagesDBHandler(getApplicationContext());
        customImages = new ArrayList<>();
        path = new Hashtable<>();

        viewImageAdapter = new gridViewImageAdapter(this, customImages);
        new AsyncTaskRunner().execute();

        gridView = (GridView) findViewById(R.id.gridView3);
        gridView.setAdapter(viewImageAdapter);

        fabButtonDown = (FloatingActionButton) findViewById(R.id.fabUp);
        fabButtonDown.setImageResource(R.drawable.add);

        fabButtonDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final long i = id;
                new AlertDialog.Builder(context)
                        .setTitle("Delete entry")
                        .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("","deleted");
                                idbh.removeImage(p.getUserID(),path.get(customImages.get((int)i)) );
                                finish();
                                startActivity(getIntent());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bm = null;
        if (data != null) {
            try {
                customImages.add(Bitmap.createBitmap(190, 190, Bitmap.Config.ARGB_4444));
                viewImageAdapter.notifyDataSetChanged();
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                String[] pathArray = data.getData().getEncodedPath().split("/");
                saveToInternalStorage(bm, pathArray[pathArray.length - 1] + "_" + p.getUserID());
                idbh.addImage(p.getUserID(), pathArray[pathArray.length - 1] + "_" + p.getUserID());
                path.put(Bitmap.createBitmap(190, 190, Bitmap.Config.ARGB_4444),pathArray[pathArray.length - 1] + "_" + p.getUserID());
                customImages.add(Bitmap.createScaledBitmap(loadImageFromStorage(pathArray[pathArray.length - 1] + "_" + p.getUserID()), 10, 10, false));
                customImages.remove(customImages.size() -2);
                viewImageAdapter.notifyDataSetChanged();
                finish();
                startActivity(getIntent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private String saveToInternalStorage(Bitmap bitmapImage, String filename) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, filename);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private Bitmap loadImageFromStorage(String path) {
        Bitmap b = null;
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File f = new File(directory, path);
            b = BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                Cursor cursorTest = idbh.getImages(p.getUserID());
                for (int i = 0; i < cursorTest.getCount(); i++) {
                    customImages.add(Bitmap.createBitmap(190, 190, Bitmap.Config.ARGB_4444));
                    viewImageAdapter.notifyDataSetChanged();
                }
                for (int i = 0; i < cursorTest.getCount(); i++) {
                    if (i == 0)
                        cursorTest.moveToPosition(i);
                    String image = cursorTest.getString(1);
                    Log.i("adapter count ", viewImageAdapter.getCount() + "");
                    if (image != "") {
                        Bitmap bit = loadImageFromStorage(image);
                        customImages.add(bit);
                        path.put(bit,image);
                        customImages.remove(0);
                    }
                    cursorTest.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            viewImageAdapter.notifyDataSetChanged();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            customImages.clear();
            viewImageAdapter.notifyDataSetChanged();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
}
