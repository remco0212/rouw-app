package com.example.remco.rouwapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Remco on 10-10-2016.
 */
public class feelingAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<Feeling> feelings = new ArrayList<Feeling>();

    public feelingAdapter(Context context, ArrayList<Feeling> feelings) {
        this.context = context;
        this.feelings = feelings;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return feelings.size();
    }

    @Override
    public Object getItem(int position) {
        return feelings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View holder = mInflator.inflate(R.layout.listview_item, parent, false);

        TextView t = (TextView) holder.findViewById(R.id.Name);
        ImageView photo = (ImageView)holder.findViewById(R.id.photo);

        Feeling feeling = feelings.get(position);
        t.setText(feeling.getDate());
        setImage(feeling.getId(),photo);
        return holder;
    }

    public void setImage(int id, ImageView imageView){
        switch(id){
            case 0:
                imageView.setImageResource(R.drawable.blij);
                break;
            case 1:
                imageView.setImageResource(R.drawable.boos);
                break;
            case 2:
                imageView.setImageResource(R.drawable.verdrietig);
                break;
            case 3:
                imageView.setImageResource(R.drawable.moe);
                break;
            case 4:
                imageView.setImageResource(R.drawable.bang);
                break;
            case 5:
                imageView.setImageResource(R.drawable.verbaasd);
                break;
            case 8:
                imageView.setImageResource(R.drawable.add_icon);
                break;
        }
    }
}