package com.example.remco.rouwapp;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Remco on 12-10-2016.
 */
public class KleurplaatAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<Kleurplaat> menuItems = new ArrayList<Kleurplaat>();

    public KleurplaatAdapter(Context context, ArrayList<Kleurplaat>  menuItems){
        this.context = context;
        this.menuItems = menuItems;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View holder;

        if(convertView == null) {
            holder = new View(this.context);
            holder = mInflator.inflate(R.layout.listview_item_info,parent,false);
        } else {
            holder = (View) convertView;
        }
        TextView t = (TextView)holder.findViewById(R.id.Name);
        ImageView photo = (ImageView)holder.findViewById(R.id.photo);
        ImageButton photoButton = (ImageButton)holder.findViewById(R.id.imageButton8);

        final Kleurplaat menuItem = menuItems.get(position);
        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage(menuItem.getSourceKleurplaat());

                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        t.setText(menuItem.getName());
        photo.setImageResource(context.getResources().getIdentifier( menuItem.getKleurplaatID(), "drawable", context.getPackageName()));
        return holder;
    }
}
