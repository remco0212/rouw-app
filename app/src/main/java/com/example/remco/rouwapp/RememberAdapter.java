package com.example.remco.rouwapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Remco on 27-10-2016.
 */
public class RememberAdapter extends BaseAdapter {

    Context context;
    LayoutInflater mInflator;
    ArrayList<Remember> remembers = new ArrayList<Remember>();

    public RememberAdapter(Context context, ArrayList<Remember> remembers) {
        this.context = context;
        this.remembers = remembers;
        mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return remembers.size();
    }

    @Override
    public Object getItem(int position) {
        return remembers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View holder = mInflator.inflate(R.layout.listview_item, parent, false);

        TextView t = (TextView) holder.findViewById(R.id.Name);
        ImageView photo = (ImageView) holder.findViewById(R.id.photo);

        Remember remember = remembers.get(position);
        if(remember != null)
        t.setText(remember.getName());
        if(position == 0)
            photo.setImageResource(R.drawable.add_icon);
        else if(remember.getImage() != "")
            photo.setImageBitmap(StringToBitMap(remember.getImage()));
        return holder;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}