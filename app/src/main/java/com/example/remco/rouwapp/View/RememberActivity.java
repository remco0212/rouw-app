package com.example.remco.rouwapp.View;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.remco.rouwapp.CustomMenuItem;
import com.example.remco.rouwapp.Feeling;
import com.example.remco.rouwapp.MenuAdapter;
import com.example.remco.rouwapp.Person;
import com.example.remco.rouwapp.R;
import com.example.remco.rouwapp.Remember;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Remco on 27-10-2016.
 */
public class RememberActivity extends AppCompatActivity {

    private Spinner spinner;
    private Context context;
    private String userID;
    private Person person;
    private Remember rememberShow;
    private ImageButton imagebutton1;
    private ImageButton imagebutton2;
    private ImageButton imagebutton3;
    private ImageButton imagebutton4;
    private ImageButton imagebutton5;
    private ImageButton imagebutton6;
    private Button addPicture;
    private Bitmap bm= null;
    private int imageID = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remember);

        Bundle bundle = getIntent().getExtras();
        userID = (String) getIntent().getExtras().getSerializable("Id");
        person = (Person) getIntent().getExtras().getSerializable("Person");
        rememberShow = (Remember) getIntent().getExtras().getSerializable("Remember");

        Button button = (Button) findViewById(R.id.button7);
        final ImageView image = (ImageView) findViewById(R.id.imageView5);
        final EditText hobby = (EditText) findViewById(R.id.editText4);
        final EditText happy = (EditText) findViewById(R.id.editText6);
        final EditText watch = (EditText) findViewById(R.id.editText7);
        final EditText funny = (EditText) findViewById(R.id.editText8);
        final EditText humor = (EditText) findViewById(R.id.editText9);
        final EditText bijzonder = (EditText) findViewById(R.id.editText10);
        final EditText together = (EditText) findViewById(R.id.editText11);
        final EditText toSay = (EditText) findViewById(R.id.editText12);
        final EditText thinkOf = (EditText) findViewById(R.id.editText13);
        final EditText stays = (EditText) findViewById(R.id.editText14);
        spinner = (Spinner) findViewById(R.id.spinner);
        imagebutton1 = (ImageButton) findViewById(R.id.imageButton2);
        imagebutton2 = (ImageButton) findViewById(R.id.imageButton3);
        imagebutton3 = (ImageButton) findViewById(R.id.imageButton4);
        imagebutton4 = (ImageButton) findViewById(R.id.imageButton5);
        imagebutton5 = (ImageButton) findViewById(R.id.imageButton6);
        imagebutton6 = (ImageButton) findViewById(R.id.imageButton7);
        addPicture = (Button) findViewById(R.id.button8);
        addPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            }
        });

        clearBackgrounds();

        imagebutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 1;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton2).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton1.setBackground(layerDraw);
            }
        });
        imagebutton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 2;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton3).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton2.setBackground(layerDraw);
            }
        });
        imagebutton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 3;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton4).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton3.setBackground(layerDraw);
            }
        });
        imagebutton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 4;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton5).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton4.setBackground(layerDraw);
            }
        });
        imagebutton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 5;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton6).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton5.setBackground(layerDraw);
            }
        });
        imagebutton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageID = 6;
                clearBackgrounds();
                Drawable drawable = (Drawable) findViewById(R.id.imageButton7).getBackground();
                ShapeDrawable shapedrawable = new ShapeDrawable();
                shapedrawable.setShape(new RectShape());
                shapedrawable.getPaint().setColor(Color.RED);
                shapedrawable.getPaint().setStrokeWidth(10f);
                shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
                Drawable drawableArray[]= new Drawable[]{shapedrawable,drawable};
                LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                layerDraw.setLayerInset(1, 5, 5, 5, 5);//set offset of 2 layer
                imagebutton6.setBackground(layerDraw);
            }
        });
        if(rememberShow != null && !rememberShow.getName().equals("Nieuwe herinnering")){
            button.setVisibility(button.INVISIBLE);
            image.setImageBitmap(StringToBitMap(rememberShow.getImage()));
            addPicture.setVisibility(Button.INVISIBLE);
            hobby.setText(rememberShow.getHobby());
            happy.setText(rememberShow.getHappy());
            watch.setText(rememberShow.getWatch());
            funny.setText(rememberShow.getFunny());
            humor.setText(rememberShow.getHumor());
            bijzonder.setText(rememberShow.getBijzonder());
            together.setText(rememberShow.getTogether());
            toSay.setText(rememberShow.getToSay());
            thinkOf.setText(rememberShow.getThinkOf());
            stays.setText(rememberShow.getStays());
            changeBackground(rememberShow.getColor());
            displayImage(rememberShow.getAnimal());
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Colors, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            if (!rememberShow.getColor().equals(null)) {
                int spinnerPosition = adapter.getPosition(rememberShow.getColor());
                spinner.setSelection(spinnerPosition);
            }
            hobby.setInputType(InputType.TYPE_NULL);
            spinner.setEnabled(false);
            hobby.setEnabled(false);
            happy.setEnabled(false);
            watch.setEnabled(false);
            funny.setEnabled(false);
            humor.setEnabled(false);
            bijzonder.setEnabled(false);
            together.setEnabled(false);
            toSay.setEnabled(false);
            thinkOf.setEnabled(false);
            stays.setEnabled(false);
        }
        else {
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    if (view != null && view instanceof TextView) {
                        ((TextView) view).setTextColor(Color.WHITE);
                        if (spinner.getSelectedItem().equals("Geel")) {
                            ((TextView) view).setTextColor(Color.BLACK);
                        }
                    }
                    changeBackground(spinner.getSelectedItem());
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                String today = formatter.format(date);

                if(bm != null) {
                    Remember remember = new Remember(Integer.parseInt(userID), today, BitMapToString(bm), hobby.getText().toString(), spinner.getSelectedItem().toString(), happy.getText().toString(), watch.getText().toString(), imageID + "", funny.getText().toString(), humor.getText().toString(), bijzonder.getText().toString(), together.getText().toString(), toSay.getText().toString(), thinkOf.getText().toString(), stays.getText().toString());
                    Intent detailIntent = new Intent(getApplicationContext(), ListviewActivity.class);
                    detailIntent.putExtra("Person", person);
                    detailIntent.putExtra("Remember", remember);
                    detailIntent.putExtra("From", "herinnering");
                    startActivity(detailIntent);
                }
                else{
                    Remember remember = new Remember(Integer.parseInt(userID), today, BitMapToString(BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.heart)), hobby.getText().toString(), spinner.getSelectedItem().toString(), happy.getText().toString(), watch.getText().toString(), imageID + "", funny.getText().toString(), humor.getText().toString(), bijzonder.getText().toString(), together.getText().toString(), toSay.getText().toString(), thinkOf.getText().toString(), stays.getText().toString());
                    Intent detailIntent = new Intent(getApplicationContext(), ListviewActivity.class);
                    detailIntent.putExtra("Person", person);
                    detailIntent.putExtra("Remember", remember);
                    detailIntent.putExtra("From", "herinnering");
                    startActivity(detailIntent);
                }
            }
        });
        context = this;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bm= null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ImageView view = (ImageView)findViewById(R.id.imageView5);
                view.setImageBitmap(bm);
                } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void clearBackgrounds(){
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        imagebutton1.setBackground(this.getResources().getDrawable(R.drawable.herinneringen1));
        imagebutton2.setBackground(this.getResources().getDrawable(R.drawable.herinneringen2));
        imagebutton3.setBackground(this.getResources().getDrawable(R.drawable.herinneringen3));
        imagebutton4.setBackground(this.getResources().getDrawable(R.drawable.herinneringen4));
        imagebutton5.setBackground(this.getResources().getDrawable(R.drawable.herinneringen5));
        imagebutton6.setBackground(this.getResources().getDrawable(R.drawable.herinneringen6));

        TableRow.LayoutParams params1 = (TableRow.LayoutParams) imagebutton1.getLayoutParams();
        params1.width = size.x/4;
        imagebutton1.setLayoutParams(params1);

        TableRow.LayoutParams params2 = (TableRow.LayoutParams) imagebutton2.getLayoutParams();
        params2.width = size.x/4;
        imagebutton2.setLayoutParams(params2);

        TableRow.LayoutParams params3 = (TableRow.LayoutParams) imagebutton3.getLayoutParams();
        params3.width = size.x/4;
        imagebutton3.setLayoutParams(params3);

        TableRow.LayoutParams params4 = (TableRow.LayoutParams) imagebutton4.getLayoutParams();
        params4.width = size.x/4;
        imagebutton4.setLayoutParams(params4);

        TableRow.LayoutParams params5 = (TableRow.LayoutParams) imagebutton5.getLayoutParams();
        params5.width = size.x/4;
        imagebutton5.setLayoutParams(params5);

        final TableRow.LayoutParams params6 = (TableRow.LayoutParams) imagebutton6.getLayoutParams();
        params6.width = size.x/4;
        imagebutton6.setLayoutParams(params6);
    }

    private void displayImage(String image){
        imagebutton1.setVisibility(Button.INVISIBLE);
        imagebutton2.setVisibility(Button.INVISIBLE);
        imagebutton3.setVisibility(Button.INVISIBLE);
        imagebutton4.setVisibility(Button.INVISIBLE);
        imagebutton5.setVisibility(Button.INVISIBLE);
        imagebutton6.setVisibility(Button.INVISIBLE);


        if(Integer.parseInt(image) == 1){
            imagebutton1.setVisibility(Button.VISIBLE);
        }else if(Integer.parseInt(image) == 2){
            imagebutton2.setVisibility(Button.VISIBLE);
        }else if(Integer.parseInt(image) == 3){
            imagebutton3.setVisibility(Button.VISIBLE);
        }else if(Integer.parseInt(image) == 4){
            imagebutton4.setVisibility(Button.VISIBLE);
        }else if(Integer.parseInt(image) == 5){
            imagebutton5.setVisibility(Button.VISIBLE);
        }else if(Integer.parseInt(image) == 6){
            imagebutton6.setVisibility(Button.VISIBLE);
        }
    }

    private void changeBackground(Object selectedItem){
        if (selectedItem.equals("Blauw")) {
            spinner.setBackgroundColor(Color.BLUE);
        } else if (selectedItem.equals("Rood")) {
            spinner.setBackgroundColor(Color.RED);
        } else if (selectedItem.equals("Groen")) {
            spinner.setBackgroundColor(Color.GREEN);
        } else if (selectedItem.equals("Geel")) {
            spinner.setBackgroundColor(Color.YELLOW);
        } else if (selectedItem.equals("Oranje")) {
            spinner.setBackgroundColor(Color.rgb(244, 127, 7));
        } else if (selectedItem.equals("Paars")) {
            spinner.setBackgroundColor(Color.rgb(192, 8, 204));
        } else if (selectedItem.equals("Roze")) {
            spinner.setBackgroundColor(Color.rgb(244, 7, 234));
        } else {
            spinner.setBackgroundColor(Color.WHITE);
        }
    }

}
